import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";
import Home from "./components/HomeComponent/Home";
import Create from "./components/CreateComponent/Create";
import GroupedByYearLevelSection from "./components/QueryAComponent/GroupedByYearLevelSection";
import GetByYearLevelSectionCityZip from "./components/QueryBComponent/GetByYearLevelSectionCityZip";
import GetByAge from "./components/QueryDComponent/GetByAge";

function App() {
  return (
    <div>
      <header>
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/create" element={<Create />} />
            <Route
              path="/groupedByYearLevelSection"
              element={<GroupedByYearLevelSection />}
            />
            <Route
              path="/getByYearLevelSectionCityZip"
              element={<GetByYearLevelSectionCityZip />}
            />
            <Route path="/getByAge" element={<GetByAge />} />
          </Routes>
        </Router>
      </header>
    </div>
  );
}

export default App;
