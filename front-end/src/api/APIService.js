import axios from "axios";

const STUDENT_API_GET = "http://localhost:8080/api/get";
const STUDENT_API_GET_PAGE = "http://localhost:8080/api/page";
const STUDENT_API_POST = "http://localhost:8080/api/add/student";
const STUDENT_API_UPDATE = "http://localhost:8080/api/update/student";
const STUDENT_API_DELETE = "http://localhost:8080/api/delete/student";

class APIService {
  getAllStudents() {
    return axios.get(STUDENT_API_GET);
  }

  getStudentById(studentId) {
    return axios.get(STUDENT_API_GET + "/student/" + studentId);
  }

  paginateAllStudents(offSet) {
    return axios.get(STUDENT_API_GET_PAGE + "/" + offSet);
  }

  addStudent(student) {
    return axios.post(STUDENT_API_POST, student);
  }

  updateStudent(student, studentId) {
    return axios.put(STUDENT_API_UPDATE + "/" + studentId, student);
  }

  deleteStudent(studentId) {
    return axios.delete(STUDENT_API_DELETE + "/" + studentId);
  }
}

export default new APIService();
