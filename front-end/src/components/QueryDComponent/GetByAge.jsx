import React from "react";
import { Button, Table, Row, Col, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./GetByAge.scss";
import APIService from "../../api/APIService";

export default class GetByAge extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      students: [],
    };
  }

  componentDidMount() {
    APIService.getAllStudents()
      .then((response) => {
        this.setState({ students: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="container">
        <hr />
        <h1>Student Information System</h1>
        <h2>
          List of all students based on age bracket let's say students ages 15
          to 18.
        </h2>
        <h3>
          {" "}
          Make sure the search can specify the age bracket to generate the
          output and should be sorted by names.
        </h3>
        <hr />
        <Row
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginLeft: "5px",
            marginRight: "0px",
          }}
        >
          <Col md={1} id="minAge">
            <Form.Control
              value={this.state.minAge}
              onChange={(e) => this.setState({ minAge: e.target.value })}
              type="text"
              placeholder="Min"
              required
            />
          </Col>
          <Col md={1} id="maxAge">
            <Form.Control
              value={this.state.maxAge}
              onChange={(e) => this.setState({ maxAge: e.target.value })}
              type="text"
              placeholder="Max"
              required
            />
          </Col>
        </Row>
        <br />
        <Table className="table table-striped table-light" bordered size="sm">
          <thead>
            <tr className="bg-primary">
              <th>Student's Name</th>
              <th>Age</th>
            </tr>
          </thead>
          <tbody>
            {/** Here we can see how the data is filtered using .filter method, sorted using the .sort method
             * and get only the necessary data that we need using the .map method.**/}
            {this.state.students
              .filter(
                (student) =>
                  student.age >= this.state.minAge &&
                  student.age <= this.state.maxAge
              )
              .sort((a, b) => {
                return a.age > b.age ? -1 : a.age < b.age ? 1 : 0;
              })
              .map((student) => (
                <tr key={student.studentId}>
                  <td>{student.studentName}</td>
                  <td style={{ fontWeight: "700" }}>{student.age}</td>
                </tr>
              ))}
          </tbody>
        </Table>

        <Link className="primary-button" to="/">
          <Button variant="primary">Home</Button>
        </Link>
      </div>
    );
  }
}
