import React, { Component } from "react";
import { Button, Form } from "react-bootstrap";
import "./Create.scss";
import APIService from "../../api/APIService";

export default class Create extends Component {
  constructor(props) {
    super(props);
    this.state = {
      studentId: null,
      studentName: "",
      age: "",
      address: "",
      zip: "",
      city: "",
      provinceAndRegion: "",
      phoneNumber: "",
      mobileNumber: "",
      email: "",
      yearLevel: "",
      section: "",
    };
  }
  addStudent = (e) => {
    let student = {
      studentId: this.state.studentId,
      studentName: this.state.studentName,
      age: this.state.age,
      address: this.state.address,
      zip: this.state.zip,
      city: this.state.city,
      provinceAndRegion: this.state.provinceAndRegion,
      phoneNumber: this.state.phoneNumber,
      mobileNumber: this.state.mobileNumber,
      email: this.state.email,
      yearLevel: this.state.yearLevel,
      section: this.state.section,
    };
    APIService.addStudent(student)
      .then((res) => {
        this.props.history.push("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  render() {
    return (
      <div className="container">
        <hr />
        <h1>Student Information System</h1>
        <h2>
          Create a new student record by filling up the form below and click the
          "Create" button.
        </h2>
        <hr />
        <Form onSubmit={this.addStudent}>
          <Form.Group controlId="formStudentName">
            <Form.Label id="formLabel">Student Name </Form.Label>
            <Form.Control
              type="text"
              name="studentName"
              placeholder=" Juan Crisostomo Magsalin Ibarra"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formAge">
            <Form.Label id="formLabel">Age</Form.Label>
            <Form.Control
              type="number"
              name="age"
              placeholder=" 25"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formAddress">
            <Form.Label id="formLabel">Address</Form.Label>
            <Form.Control
              type="text"
              name="address"
              placeholder=" Francisco Mercado St. corner Jose P. Rizal Street, Barangay 5, Poblacion, Calamba, Laguna"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formZip">
            <Form.Label id="formLabel">Zip</Form.Label>
            <Form.Control
              type="text"
              name="zip"
              placeholder=" 4027"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formCity">
            <Form.Label id="formLabel">City</Form.Label>
            <Form.Control
              type="text"
              name="city"
              placeholder=" City of Calamba"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formProvinceAndRegion">
            <Form.Label id="formLabel">Province and Region</Form.Label>
            <Form.Control
              type="text"
              name="provinceAndRegion"
              placeholder=" Laguna, IV-A (Calabarzon)"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formPhoneNumber">
            <Form.Label id="formLabel">Phone Number</Form.Label>
            <Form.Control
              type="text"
              name="phoneNumber"
              placeholder=" +63 912 345 6789"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formMobileNumber">
            <Form.Label id="formLabel">Mobile Number</Form.Label>
            <Form.Control
              type="text"
              name="mobileNumber"
              placeholder=" +63 912 345 6789"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formEmail">
            <Form.Label id="formLabel">Email </Form.Label>
            <Form.Control
              type="text"
              name="email"
              placeholder=" jc.ibarra@myemail.com"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formYearLevel">
            <Form.Label id="formLabel">Year Level</Form.Label>
            <Form.Control
              type="number"
              name="yearLevel"
              placeholder=" 1"
              onChange={this.handleChange}
            />
          </Form.Group>
          <br />
          <Form.Group controlId="formSection">
            <Form.Label id="formLabel">Section</Form.Label>
            <Form.Control
              type="text"
              name="section"
              placeholder=" Saint John"
              onChange={this.handleChange}
            />
          </Form.Group>
          <hr />
          <div className="d-grid gap-2">
            <Button
              variant="primary"
              type="submit"
              disabled={
                !this.state.studentName ||
                !this.state.age ||
                !this.state.address ||
                !this.state.zip ||
                !this.state.city ||
                !this.state.provinceAndRegion ||
                !this.state.phoneNumber ||
                !this.state.mobileNumber ||
                !this.state.email ||
                !this.state.yearLevel ||
                !this.state.section
              }
            >
              Submit
            </Button>
            <Button
              variant="outline-primary"
              type="reset"
              href="http://localhost:3000/"
              id="returnHome"
            >
              Back
            </Button>
          </div>
          <hr />
        </Form>
      </div>
    );
  }
}
