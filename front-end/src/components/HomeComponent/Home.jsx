import React from "react";
import {
  Button,
  ButtonGroup,
  Table,
  Modal,
  Form,
  CloseButton,
  Dropdown,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Home.scss";
import APIService from "../../api/APIService";

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      students: [],
      studentId: "",
      studentName: "",
      age: "",
      address: "",
      zip: "",
      city: "",
      provinceAndRegion: "",
      phoneNumber: "",
      mobileNumber: "",
      email: "",
      yearLevel: "",
      section: "",
      show: false,
      page: props.page,
      pageSize: props.pageSize,
      currentPage: 0,
    };
    this.deleteStudent = this.deleteStudent.bind(this);
    this.toFirstPage = this.toFirstPage.bind(this);
    this.toPreviousPage = this.toPreviousPage.bind(this);
    this.toNextPage = this.toNextPage.bind(this);
    this.listAllStudents = this.listAllStudents.bind(this);
  }

  // this used to connect and check if our front-end now connects to our back-end using the APIService
  componentDidMount() {
    APIService.paginateAllStudents(this.state.currentPage).then((response) => {
      this.setState({ students: response.data });
    });
  }

  // this handles the close state and sets its property to false once it is called.
  handleClose = () => {
    this.setState({ show: false });
  };

  // this handles the show state and sets its property to false and the values to be displayes once it is called.
  handleShow = (student) => {
    this.setState({
      show: true,
      studentId: student.studentId,
      studentName: student.studentName,
      age: student.age,
      address: student.address,
      zip: student.zip,
      city: student.city,
      provinceAndRegion: student.provinceAndRegion,
      phoneNumber: student.phoneNumber,
      mobileNumber: student.mobileNumber,
      email: student.email,
      yearLevel: student.yearLevel,
      section: student.section,
    });
  };

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  // this handles the update method runs similarly as the addStudent method that we have seen earlier.
  // except that the update student method already fetches first the data from our database
  updateStudent = (event) => {
    const student = {
      studentId: this.state.studentId,
      studentName: this.state.studentName,
      age: this.state.age,
      address: this.state.address,
      zip: this.state.zip,
      city: this.state.city,
      provinceAndRegion: this.state.provinceAndRegion,
      phoneNumber: this.state.phoneNumber,
      mobileNumber: this.state.mobileNumber,
      email: this.state.email,
      yearLevel: this.state.yearLevel,
      section: this.state.section,
    };
    APIService.updateStudent(student, student.studentId).then((response) => {
      this.props.history.push("/");
    });
  };

  // this method is to delete a student by specifying their studentID
  // we also used APIService so that we can process this using the back-end
  deleteStudent(studentId) {
    APIService.deleteStudent(studentId).then((response) => {
      this.setState({
        students: this.state.students.filter(
          (student) => student.id !== studentId
        ),
      });
    });
  }

  // this method is to paginate the students. We used the currentPage property to paginate the students
  // we used the APIService to process this using the back-end
  toFirstPage() {
    this.setState({ currentPage: 0 });
    APIService.paginateAllStudents(0).then((response) => {
      this.setState({ students: response.data });
    });
  }

  toPreviousPage() {
    this.setState({ currentPage: this.state.currentPage - 1 });
    APIService.paginateAllStudents(this.state.currentPage - 1).then(
      (response) => {
        this.setState({ students: response.data });
      }
    );
  }

  toNextPage() {
    this.setState({ currentPage: this.state.currentPage + 1 });
    APIService.paginateAllStudents(this.state.currentPage + 1).then(
      (response) => {
        this.setState({ students: response.data });
      }
    );
  }

  // this method is to list all the students. We used the APIService to process this using the back-end.
  listAllStudents() {
    APIService.getAllStudents().then((response) => {
      this.setState({ students: response.data });
    });
  }

  render() {
    return (
      <div className="container">
        <hr />
        <h1>Student Information System</h1>
        <hr />
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Age</th>
              <th>Address</th>
              <th>Zip</th>
              <th>City</th>
              <th>Province and Region</th>
              <th>Phone Number</th>
              <th>Mobile Number</th>
              <th>Email Address</th>
              <th>Year Level</th>
              <th>Section</th>
              <th>
                <Dropdown as={ButtonGroup} size="sm">
                  <Button variant="primary" href="/create">
                    Create New Student
                  </Button>
                  <Dropdown.Toggle variant="primary"></Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item onClick={this.listAllStudents}>
                      <p>List All Students</p>
                    </Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item href="/groupedbyyearlevelsection">
                      <p>Grouped by Year Level and Section</p>
                    </Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item href="/getbyyearlevelsectioncityzip">
                      <p>Get by Year Level, Section, City, and Zip</p>
                    </Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item href="/getbyage">
                      <p>Get by Age</p>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.students
              .sort((a, b) => a.studentId - b.studentId)
              .map((student) => (
                <tr key={student.studentId}>
                  <td style={{ fontWeight: "700" }}>{student.studentId}</td>
                  <td>{student.studentName}</td>
                  <td>{student.age}</td>
                  <td>{student.address}</td>
                  <td>{student.zip}</td>
                  <td>{student.city}</td>
                  <td>{student.provinceAndRegion}</td>
                  <td>{student.phoneNumber}</td>
                  <td>{student.mobileNumber}</td>
                  <td>{student.email}</td>
                  <td>{student.yearLevel}</td>
                  <td>{student.section}</td>
                  <td>
                    <ButtonGroup vertical>
                      <Button
                        size="sm"
                        variant="primary"
                        onClick={() => this.handleShow(student)}
                      >
                        Edit
                      </Button>
                      <Button
                        size="sm"
                        variant="danger"
                        onClick={() => this.deleteStudent(student.studentId)}
                        href="http://localhost:3000/"
                      >
                        Delete
                      </Button>
                    </ButtonGroup>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
        <Table responsive>
          <th>
            <ButtonGroup>
              <Button
                variant="outline-primary"
                onClick={this.toFirstPage}
                size="sm"
                disabled={
                  this.state.currentPage === 0 || this.state.currentPage === 1
                }
              >
                First
              </Button>
              <Button
                variant="outline-primary"
                onClick={this.toPreviousPage}
                size="sm"
                disabled={this.state.currentPage === 0}
              >
                Previous
              </Button>
              <Button
                variant="outline-primary"
                onClick={this.toNextPage}
                size="sm"
                disabled={this.state.students.length === 0}
              >
                Next
              </Button>
            </ButtonGroup>
          </th>
          <h2>Page {this.state.currentPage + 1}</h2>
        </Table>
        <hr />
        {/** when edit button is clicked this modal shows up and calls 
           updateStudent method when update button is clicked **/}
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header>
            <Modal.Title>Edit Student</Modal.Title>
            <CloseButton
              variant="white"
              aria-label="Hide"
              onClick={this.handleClose}
            />
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={this.updateStudent}>
              <Form.Group className="mb-3" controlId="formStudentId">
                <Form.Label>Student ID</Form.Label>
                <Form.Control
                  style={{ fontWeight: "700" }}
                  id="tableContent"
                  name="studentId"
                  placeholder={this.state.studentId}
                  readOnly
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicName">
                <Form.Label>Student Name</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="studentName"
                  value={this.state.studentName}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formAge">
                <Form.Label>Age</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="age"
                  value={this.state.age}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formAddress">
                <Form.Label>Address</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="address"
                  value={this.state.address}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formZip">
                <Form.Label>Zip</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="zip"
                  value={this.state.zip}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formCity">
                <Form.Label>City</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="city"
                  value={this.state.city}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formProvinceAndRegion">
                <Form.Label>Province and Region</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="provinceAndRegion"
                  value={this.state.provinceAndRegion}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formPhoneNumber">
                <Form.Label>Phone Number</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="phoneNumber"
                  value={this.state.phoneNumber}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formMobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="mobileNumber"
                  value={this.state.mobileNumber}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formEmail">
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formYearLevel">
                <Form.Label>Year Level</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="yearLevel"
                  value={this.state.yearLevel}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formSection">
                <Form.Label>Section</Form.Label>
                <Form.Control
                  id="tableContent"
                  type="text"
                  name="section"
                  value={this.state.section}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onClick={this.handleClose}
                disabled={
                  !this.state.studentName ||
                  !this.state.age ||
                  !this.state.address ||
                  !this.state.zip ||
                  !this.state.city ||
                  !this.state.provinceAndRegion ||
                  !this.state.phoneNumber ||
                  !this.state.mobileNumber ||
                  !this.state.email ||
                  !this.state.yearLevel ||
                  !this.state.section
                }
              >
                Update
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
