import React from "react";
import { Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./GetByYearLevelSectionCityZip.scss";
import APIService from "../../api/APIService";

export default class GetByYearLevelSectionCityZip extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      students: [],
    };
  }

  componentDidMount() {
    APIService.getAllStudents()
      .then((response) => {
        this.setState({ students: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="container">
        <hr />
        <h1>Student Information System</h1>
        <h2>
          List all students per year level, section, city, and zip sorted by
          name
        </h2>
        <hr />
        <Table className="table table-striped table-light" bordered size="sm">
          <thead>
            <tr className="bg-primary">
              <th>Name</th>
              <th>Year Level</th>
              <th>Section</th>
              <th>City</th>
              <th>Zip</th>
            </tr>
          </thead>
          <tbody>
            {this.state.students
              .sort((a, b) => a.studentId - b.studentId)
              .map((student) => (
                <tr key={student.studentId}>
                  <td>{student.studentName}</td>
                  <td>{student.yearLevel}</td>
                  <td>{student.section}</td>
                  <td>{student.city}</td>
                  <td>{student.zip}</td>
                </tr>
              ))}
          </tbody>
        </Table>

        <Link className="primary-button" to="/">
          <Button variant="primary">Home</Button>
        </Link>
      </div>
    );
  }
}
