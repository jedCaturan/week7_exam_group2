import React from "react";
import { Button, Table, Col, Row, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./GroupedByYearLevelSection.scss";
import APIService from "../../api/APIService";

export default class GroupedByYearLevelSection extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      students: [],
    };
  }

  // this method is to get the data that is processed in the back end.
  // also checks if connection using the API Service is successful.
  componentDidMount() {
    APIService.getAllStudents()
      .then((response) => {
        this.setState({ students: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="container">
        <hr />
        <h1>Student Information System</h1>
        <h2>
          List all students that can be sorted by names and grouped by year
          level and section
        </h2>
        <hr />
        <Row
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginLeft: "5px",
            marginRight: "0px",
          }}
        >
          <Col md={2} id="yearLevel">
            <Form.Control
              value={this.state.yearLevel}
              onChange={(e) => this.setState({ yearLevel: e.target.value })}
              type="text"
              placeholder="Year Level"
              required
            />
          </Col>
        </Row>
        <br />
        <Table className="table table-striped table-light" bordered size="sm">
          <thead>
            <tr className="bg-primary">
              <th>Section</th>
              <th>Name</th>
              <th>Age</th>
              <th>Address</th>
              <th>Zip</th>
              <th>City</th>
              <th>Province and Region</th>
              <th>Phone Number</th>
              <th>Mobile Number</th>
              <th>Email Address</th>
            </tr>
          </thead>
          <tbody>
            {this.state.students
              .filter((student) => student.yearLevel == this.state.yearLevel)
              .sort((a, b) => a.section.localeCompare(b.section))
              .map((student) => (
                <tr key={student.id}>
                  <td style={{ fontWeight: "700" }}>{student.section}</td>
                  <td>{student.studentName}</td>
                  <td>{student.age}</td>
                  <td>{student.address}</td>
                  <td>{student.zip}</td>
                  <td>{student.city}</td>
                  <td>{student.provinceAndRegion}</td>
                  <td>{student.phoneNumber}</td>
                  <td>{student.mobileNumber}</td>
                  <td>{student.email}</td>
                </tr>
              ))}
          </tbody>
        </Table>

        <Link className="primary-button" to="/">
          <Button variant="primary">Home</Button>
        </Link>
      </div>
    );
  }
}
