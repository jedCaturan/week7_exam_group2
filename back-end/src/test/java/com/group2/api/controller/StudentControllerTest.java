package com.group2.api.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.group2.api.model.Student;
import com.group2.api.repository.StudentRepository;
import com.group2.api.services.StudentService;

@WebMvcTest(StudentController.class)
class StudentControllerTest {

        @Autowired
        private MockMvc mockMvc;

        @MockBean
        StudentService studentService;

        @MockBean
        StudentRepository studentRepository;

        @Autowired
        private ObjectMapper objectMapper = new ObjectMapper();

        final Student RECORD_1 = new Student(1l, "Henry Dean Cornico", 22, "Imus", "3456", "Imus", "Cavite, IV-A",
                        "+63 900 000 0000", "+63 900 000 0000", "henrydeancornico@email.com", 2,
                        "Saint Therese");
        final Student RECORD_2 = new Student(2l, "Airon Eusebio", 22, "Bulacan", "3456", "Bulacan",
                        "Bulacan, Central Luzon",
                        "+63 900 000 0000", "+63 900 000 0000", "aironeusebio@email.com", 2,
                        "Saint Therese");
        final Student RECORD_3 = new Student(3l, "Michael Gelle", 26, "Dasma", "3456", "Dasma", "Cavite, IV-A",
                        "+63 900 000 0000", "+63 900 000 0000", "michaelgelle@email.com", 2,
                        "Saint Therese");

        @Test
        void givenStudentIsCreated_whenStudentServiceCreateStudent_thenReturnSuccess() throws Exception {
                Student student = Student.builder()
                                .studentName("Dean Cornico")
                                .age(31)
                                .address("Imus")
                                .zip("3456")
                                .city("imus")
                                .provinceAndRegion("Cavite, IV-A")
                                .phoneNumber("+63 900 000 0000")
                                .mobileNumber("+63 900 000 0000")
                                .email("deancornico@email.com")
                                .section("Saint Therese")
                                .build();

                Mockito.when(studentService.createStudent(student)).thenReturn(student);

                MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/students/addStudent")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(this.objectMapper.writeValueAsString(student));

                mockMvc.perform(mockRequest)
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$", notNullValue()))
                                .andExpect(jsonPath("$.studentName", is("Dean Cornico")));

        }

        @Test
        void givenListStudent_whenStudentServiceGetAllStudent_thenReturnSuccess() throws Exception {
                List<Student> students = new ArrayList<>(Arrays.asList(RECORD_1,
                                RECORD_2, RECORD_3));

                Mockito.when(studentService.getAllStudent()).thenReturn(students);

                mockMvc.perform(MockMvcRequestBuilders
                                .get("/api/students")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$", hasSize(3)))
                                .andExpect(jsonPath("$[0].studentName", is("Henry Dean Cornico")))
                                .andExpect(jsonPath("$[1].studentName", is("Airon Eusebio")))
                                .andExpect(jsonPath("$[2].studentName", is("Michael Gelle")));
        }

        @Test
        void givenStudentById_whenStudentServiceGetStudentById_ThenReturnSuccess() throws Exception {
                Mockito.when(studentService.getStudentById(RECORD_2.getStudentId())).thenReturn(RECORD_2);

                mockMvc.perform(MockMvcRequestBuilders
                                .get("/api/students/getStudentById/2")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$", notNullValue()))
                                .andExpect(jsonPath("$.studentName", is("Airon Eusebio")));
        }

        @Test
        void givenStudentIsUpdated_whenStudentServiceUpdateStudent_thenReturnSuccess() throws Exception {

                Student updatedRecord = Student.builder()
                                .studentName("Dean Cornico")
                                .age(31)
                                .address("Imus")
                                .zip("3456")
                                .city("imus")
                                .provinceAndRegion("Cavite, IV-A")
                                .phoneNumber("+63 900 000 0000")
                                .mobileNumber("+63 900 000 0000")
                                .email("deancornico@email.com")
                                .yearLevel(2)
                                .section("Saint Therese")
                                .build();

                Mockito.when(studentService.getStudentById(RECORD_1.getStudentId())).thenReturn((RECORD_1));
                Mockito.when(studentService.updateStudent(RECORD_1.getStudentId(), updatedRecord))
                                .thenReturn(updatedRecord);

                MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/students/updateStudent/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(updatedRecord));

                mockMvc.perform(mockRequest)
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$", notNullValue()))
                                .andExpect(jsonPath("$.studentName", is("Dean Cornico")));

        }

        @Test
        void givenStudentIsDeleted_whenStudentServiceDeleteStudent_thenReturnSuccess() throws Exception {
                Mockito.when(studentService.getStudentById(RECORD_2.getStudentId())).thenReturn(RECORD_2);

                mockMvc.perform(MockMvcRequestBuilders
                                .delete("/api/students/deleteStudent/2")
                                .contentType(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk());
        }
}