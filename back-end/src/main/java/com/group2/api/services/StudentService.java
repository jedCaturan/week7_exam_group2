package com.group2.api.services;

import java.util.List;

import org.springframework.data.domain.Page;

import com.group2.api.model.Student;

/**
 * The StudentService Interface Class
 * <p>
 * This class imposes only the methods that
 * </p>
 * <p>
 * are used in this class. This class will then be implemented
 * </p>
 * <p>
 * in the StudentServiceImpl Implementation class.
 * </p>
 * 
 * @author Group 2
 */
public interface StudentService {

    // the student service class defines all the methods that we need to impose on
    // our implementation class.
    Student createStudent(Student student);

    Page<Student> paginateAllStudent(int offSet);

    List<Student> getAllStudent();

    Student getStudentById(Long studentId);

    Student updateStudent(Long studentId, Student student);

    boolean deleteStudent(Long studentId);

    List<Object[]> getByYearLevelSection();

    List<Object[]> getByYearLevelSectionCityZip();

    List<Object[]> getByAge(int minAge, int maxAge);
}