package com.group2.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Student Model Class
 * <p>
 * This class handles the information that can be passed
 * </p>
 * <p>
 * to the methods that are in the StudentService class.
 * </p>
 * 
 * @author Group 2
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "students", uniqueConstraints = {
        @UniqueConstraint(name = "student_email_unique", columnNames = "email")
})
public class Student {

    // the entity class is responsible for representing data that can be persisted
    // to the database in order to use the functions that are defined in the student
    // service class.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "Unique id of the students")
    private long studentId;

    @ApiModelProperty(notes = "Student's name")
    private String studentName;

    @ApiModelProperty(notes = "Student's age")
    private Integer age;

    @ApiModelProperty(notes = "Student's address")
    private String address;

    @ApiModelProperty(notes = "Student's zip")
    private String zip;

    @ApiModelProperty(notes = "Student's city")
    private String city;

    @ApiModelProperty(notes = "Student's province and region")
    private String provinceAndRegion;

    @ApiModelProperty(notes = "Student's phone number")
    private String phoneNumber;

    @ApiModelProperty(notes = "Student's mobile number")
    private String mobileNumber;

    @ApiModelProperty(notes = "Student's email")
    private String email;

    @ApiModelProperty(notes = "Student's year level")
    private Integer yearLevel;

    @ApiModelProperty(notes = "Student's section")
    private String section;
}