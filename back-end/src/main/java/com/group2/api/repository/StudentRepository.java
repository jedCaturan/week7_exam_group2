package com.group2.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.group2.api.model.Student;

/**
 * The Student Repository Class
 * <p>
 * This class extends the use of JpaRepository class.
 * </p>
 * <p>
 * This class handles the queries either pre-defined or special
 * </p>
 * <p>
 * that are going to be used inside or Service Implementation Class
 * </p>
 * 
 * @see com.group2.api.services.StudentServiceImpl
 * @author Group 2
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    // the student repository class is what we can use to either define special
    // queries or use their already available pre-defined functions to sort or
    // filter the data that we need to get.

    @Query(value = "SELECT * FROM Students students WHERE students.section IN (SELECT students.section FROM Students students GROUP BY students.section);", nativeQuery = true)
    List<Object[]> getByYearLevelSection();

    @Query(value = "SELECT students.student_name, students.year_level, students.section, students.city, students.zip FROM Students students ORDER BY students.student_name ASC;", nativeQuery = true)
    List<Object[]> getByYearLevelSectionCityZip();

    @Query(value = "SELECT students.student_name, students.age FROM Students students WHERE students.age BETWEEN ?1 AND ?2 ORDER BY students.student_name ASC;", nativeQuery = true)
    List<Object[]> getByAge(int minAge, int maxAge);

}