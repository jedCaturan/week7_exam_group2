package com.group2.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.group2.api.model.Student;
import com.group2.api.services.StudentService;

import io.swagger.annotations.ApiOperation;

/**
 * The StudentController Class
 * <p>
 * This class handles the mapping of the URL.
 * </p>
 * <p>
 * This is where the authors defines the endpoints needed to be specified to
 * call a
 * </p>
 * <p>
 * certain method.
 * </p>
 * 
 * @author Group 2
 * @see StudentService this class makes the calls for the methods that were
 *      overriden in the implementation class.
 */
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/api")
public class StudentController {

    @Autowired
    StudentService studentService;

    /**
     * Constructor
     * <p>
     * This is the constructor method it takes the parameter StudentService class.
     * </p>
     * 
     * @param studentService is then used to call the methods needed for each
     *                       endpoint.
     *                       <p>
     *                       Note that we also created our own instance of the
     *                       StudentService class.
     *                       </p>
     */
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    /**
     * Returns the createStudent method
     * <p>
     * By calling the /add/student endpoint
     * </p>
     * 
     * @param student is the model class that we have to pass in the parameter
     *                <p>
     *                this contains the student information that is needed to create
     *                a new student.
     *                </p>
     * @return studentService.createStudent
     */
    @PostMapping(value = "/add/student")
    @ApiOperation(value = "Add a new student record", notes = "Add a new student record", response = Student.class)
    public Student createStudent(@RequestBody Student student) {
        return studentService.createStudent(student);
    }

    /**
     * Returns the student list by calling the paginateAllStudent method
     * <p>
     * By calling the /page/{offSet} endpoint
     * </p>
     * <p>
     * This method paginates the list of all the students by using the Page and List
     * class
     * </p>
     * 
     * @param offSet to set the amount of data that is going to be displayed per
     *               page
     * @return studentList is the resulting list to contain the contents of
     *         studentPage.
     */
    @GetMapping(value = "/page/{offSet}")
    @ApiOperation(value = "Get the list of all students by page", notes = "Get the list of all students by page", response = Student.class)
    public List<Student> paginateAllStudent(@PathVariable int offSet) {
        Page<Student> studentPage = studentService.paginateAllStudent(offSet);
        List<Student> studentList = studentPage.getContent();
        return studentList;
    }

    /**
     * Returns the list of all students.
     * <p>
     * by calling the /get endpoint this will list all the student information
     * </p>
     * <p>
     * that is in the database.
     * </p>
     * 
     * @return student list
     */
    @GetMapping(value = "/get")
    @ApiOperation(value = "Get the list of all students", notes = "Get all students", response = Student.class)
    public List<Student> getAllStudent() {
        return studentService.getAllStudent();
    }

    /**
     * Returns all information from a specific student
     * <p>
     * This method returns the information of a student by specifying its ID.
     * </p>
     * 
     * @param studentId used to identify which student information to get.
     * @return studentService.getStudentById(studentId) all the information of a
     *         specific student.
     */
    @GetMapping(value = "/get/student/{studentId}")
    @ApiOperation(value = "Get student record by ID", notes = "Get student record by ID", response = Student.class)
    public Student getStudentById(@PathVariable Long studentId) {
        return studentService.getStudentById(studentId);
    }

    /**
     * Returns an update on an existsing student information
     * <p>
     * By calling /update/student/{studentId} this will
     * </p>
     * <p>
     * use the updateStudent method from the student service class to update an
     * existing
     * </p>
     * <p>
     * student record.
     * </p>
     * 
     * @param studentId specifies which student record to update
     * @param student   to get the existing student record.
     * @return response
     */
    @PutMapping(value = "/update/student/{studentId}")
    @ApiOperation(value = "Update student record", notes = "Update student record", response = Student.class)
    public ResponseEntity<Student> updateStudent(@PathVariable Long studentId,
            @RequestBody Student student) {
        student = studentService.updateStudent(studentId, student);
        return ResponseEntity.ok(student);
    }

    /**
     * Deletes a specific student in the database.
     * <p>
     * By calling the /delete/student/{studentId} endpoint
     * </p>
     * <p>
     * this will delete a student and all their information in the database.
     * </p>
     * 
     * @param studentId used to identify which student to delete.
     * @return response - deleted.
     */
    @DeleteMapping(value = "/delete/student/{studentId}")
    @ApiOperation(value = "Delete student record", notes = "Delete student record", response = Student.class)
    public ResponseEntity<Map<String, Boolean>> deleteStudent(@PathVariable Long studentId) {
        boolean deleted = false;
        deleted = studentService.deleteStudent(studentId);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", deleted);
        return ResponseEntity.ok(response);
    }

    /**
     * Returns a filtered list
     * <p>
     * By calling the /get/by-year-level-section
     * </p>
     * <p>
     * this will call the getByYearLevelSection method from
     * </p>
     * <p>
     * the service class and group the list by year level and section
     * </p>
     * 
     * @return studentService.getByYearLevelSection()
     */
    @GetMapping(value = "/get/by-year-level-section")
    @ApiOperation(value = "List of all students that are grouped by year level and section", notes = "List of all students that are grouped by year level and section", response = Student.class)
    public List<Object[]> getByYearLevelSection() {
        return studentService.getByYearLevelSection();
    }

    /**
     * Returns a filtered list.
     * <p>
     * By calling /get/by-year-level-section-city-zip endpoint
     * </p>
     * <p>
     * this will call the getByYearLevelSectionCityZip and return a list
     * </p>
     * <p>
     * of student that is filtered and sorted by year-level, section, city and zip
     * </p>
     * 
     * @return studentService.getByYearLevelSectionCityZip()
     */
    @GetMapping(value = "/get/by-year-level-section-city-zip")
    @ApiOperation(value = "List of all students per year level, section, city, and zip", notes = "List of all students per year level, section, city, and zip", response = Student.class)
    public List<Object[]> getByYearLevelSectionCityZip() {
        return studentService.getByYearLevelSectionCityZip();
    }

    /**
     * Returns a filtered list.
     * <p>
     * By calling the /get/by-age/{minAge}/{maxAge}
     * </p>
     * <p>
     * this will return a list that is filtered only by and between the given ages.
     * </p>
     * 
     * @param minAge this parameter specifies the <b>minimum</b> age that the system
     *               needs to get.
     * @param maxAge this parameter specifies the <b>maximum </b> age that the
     *               system needs to get.
     * @return studentService.getByAge(minAge, maxAge)
     */
    @GetMapping(value = "/get/by-age/{minAge}/{maxAge}")
    @ApiOperation(value = "List of all students based on age bracket", notes = "List of all students based on age bracket", response = Student.class)
    public List<Object[]> getByAge(@PathVariable("minAge") int minAge, @PathVariable("maxAge") int maxAge) {
        return studentService.getByAge(minAge, maxAge);
    }
}